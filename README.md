# Goon Flight Assessment Program (GoonFAP)

This program reads event data exported from Tacview and references involved units against known unit cost data to determine the cost effectiveness of the actors in the Tacview recording (typically, a Goon mission package).

## To Build

This project requires JDK11 (minimum) and Maven.

From the root directory of this project, execute `mvn clean package`.  The rest will take care of itself.

## To Run

### Prepare an input file

1. Open Tacview with the mission you want to evaluate.
2. File (Picture of USB Drive) -> Export Flight Log...
3. In the save window, save as "UTF-8 comma-separated values Flight Log (*.csv), then save to a location of your choice.
4. Because Tacview is a jerk and doesn't save a CSV with consistent column data, open your CSV in your spreadsheet program of choice (I used LibreOffice Calc).
5. Save your sheet as a CSV again and inspect it in a text editor to make sure that the number of columns match the number of headers.
6. You are now ready to run the program.

### Go forth and FAP

```text
Usage: GoonFAP [-hV] [-d=<costdbfile>] <missionlog>
Accepts a CSV flight plan exported from TacView to evaluate AirGoons weekly DCS
mission performance.
      <missionlog>   The mission log to be processed.
  -d, --database=<costdbfile>
                     The cost database used to get prices for units in the
                       mission.
  -h, --help         Show this help message and exit.
  -V, --version      Print version information and exit.
```

For most use cases, `java -jar target\goonfap-1.0-SNAPSHOT.jar <Path_To_Input.csv>` is sufficient.  If you wish to use a custom cost database, you may use `-d` or `--database=` to select a custom file.  The report will be printed to Standard I\O, you may append `> report.txt` to save it as a text file.

## The Cost Database

The cost database is a CSV file used by GoonFAP to determine what the unit cost of each unit is.  These prices are most frequently pulled from Wikipedia and scaled for inflation, but since military cost data isn't always publicly known, there are plenty of educated guesses as well.

Since the cost database is a work in progress, you may find yourself assessing a mission that the costdb lacks information on.  Notifications about this situation appears at the top of the report, `Unit of type <Unit Name> not found in database.`  To fix, you may modify the cost DB to include `<Unit Name>` and its cost, or use a custom cost database.

The cost database format is:

```text
name,type,cost
A-10C Thunderbolt II,aircraft,46300000.00
```

`name` corresponds to the string that identifies the unit in the event log.  The `not found in database` message indicates what that string is, if you are adding a missing unit.  The sample unit is an `A-10C Thunderbolt II`
`type` presently has no function and may be an arbitrary string.  Ideally, make it describe the general type of unit you are adding to the database.  The sample unit is an `aircraft`
`cost` the unit price of the unit in question.  The sample unit costs $46,300,000.00

## Report Format

If any units are missing from the cost database, you will be notified before the report.

```text
Unit of type UH-1H Huey not found in database.
Unit of type weapons.missiles.AGR_20_M282 not found in database.
Unit of type weapons.missiles.AGR_20A not found in database.
Unit of type weapons.bombs.CBU_105 not found in database.
Unit of type AIM-54A Phoenix not found in database.
Unit of type AGM-65 Maverick not found in database.
```

Any teamkills that occurred will be listed first, with the time since recording start that the incident occurred, the horrible jerk that committed the fratricide, and the poor innocent victim listed.

```text
=Goon Flight Assessment Program (FAP) Report:
==TEAMKILLS:
At 3731.84s, SNARE DRUM 1-1 | ELMO killed a Soldier M4 piloted by
```

The coalition assessment lists how much in assets *acted* during the operation (note that units on the map, but did not log an event aren't counted), how much value was expended by named pilots in munition fires and deaths, and how much was destroyed by pilot action.

```text
==Bluefor:
Committed $730,141,896.70 in assets to the operation, expending $167,487,634.91 to destroy $561,750,664.05
```

Each named pilot, be they players or AI have their individual stats for the mission listed.
`Killed` sums all targets the pilot has been listed as the cause of death in a `HasBeenDestroyed` event and their collective value.
`Expended` sums all munitions for which a `HasFired` event has occurred.  Depending on the module, this may include ejections and dropped stores.  It also counts deaths, which is calculated as `HasTakenOff` - `HasLanded` events.  The reason it is calculated this way instead of using `HasBeenDestroyed` events is that logging off also triggers those events, incorrectly making logging off look like death.
`Munitions Employed` lists all fired munitions summed in `Expended`, as well as how many actually hit their target (i.e. struck something in a `HasBeenHitBy` event)

```text
===Pilot: QUICKDRAW 1 | Arbitrary
        Killed: 1 targets valued at $272,419,567.67
        Expended: $83,415,456.22 over the course of the mission, including 1 deaths.
        Munitions Employed:
                weapons.shells.M53_APT_RED Fired:45 Hit:0
                weapons.shells.M56A3_HE_RED Fired:223 Hit:1
                AIM-54A Phoenix Fired:2 Hit:1
                AIM-9M Sidewinder Fired:2 Hit:0
```

## Known Issues
* The costdb still lacks many units.
* Whoever runs the GoonFAP report must provide a continuous Tacview recording from before the first Goon takes off to until the last Goon lands.  Stopping recording or logging off before all goons land will incorrectly tally deaths for all goons still in the air at the end of the recording.