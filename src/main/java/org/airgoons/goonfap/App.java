package org.airgoons.goonfap;

import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import org.airgoons.goonfap.database.UnitDatabase;
import org.airgoons.goonfap.missiondata.MissionRecord;
import org.airgoons.goonfap.missiondata.MissionReportBuilder;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(name = "GoonFAP", mixinStandardHelpOptions = true, description = "Accepts a CSV flight plan exported from TacView to evaluate AirGoons weekly DCS mission performance.")
public class App implements Callable<Integer>
{
	@Option(names={"-d", "--database"}, description = "The cost database used to get prices for units in the mission.")
	private Path costdbfile;
	@Parameters(index="0", description = "The mission log to be processed.")
	private Path missionlog;
	
	
    public static void main( String[] args )
    {
    	int exitCode = new CommandLine(new App()).execute(args);
    	System.exit(exitCode);
        
    }


	@Override
	public Integer call() throws Exception {
		//Load Cost Database 
		UnitDatabase unitdb = new UnitDatabase();
		if(costdbfile == null) {
			unitdb.load(App.class.getResourceAsStream("/costdb.csv"));
		} else {
			unitdb.load(costdbfile);
		}

    	//Build the Report
		CsvToBeanBuilder<MissionRecord> readerbuilder = new CsvToBeanBuilder<>(new CSVReader(new InputStreamReader(Files.newInputStream(missionlog))));
		CsvToBean<MissionRecord	> reader = readerbuilder.withType(MissionRecord.class).build();
		List<MissionRecord> recordlist = reader.stream().collect(Collectors.toList());
		MissionReportBuilder mrb = new MissionReportBuilder(unitdb, recordlist);
		System.out.println(mrb.buildReportAll());
		return 0;
	}
    
    
}
