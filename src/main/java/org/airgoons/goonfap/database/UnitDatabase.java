package org.airgoons.goonfap.database;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;



public class UnitDatabase {
	private Map<String,UnitEntry> units;
	
	public UnitDatabase() {
		units = new HashMap<String,UnitEntry>();
	}
	
	public void load(InputStream stream) throws IOException {
		CsvToBeanBuilder<UnitEntry> readerbuilder = new CsvToBeanBuilder<>(new CSVReader(new InputStreamReader(stream)));
		CsvToBean<UnitEntry> reader = readerbuilder.withType(UnitEntry.class).build();
		units = reader.stream().collect(Collectors.toMap( UnitEntry::getName, Function.identity()));
	}
	
	public void load(Path file) throws IOException {
		load(Files.newInputStream(file));
	}
	
	public UnitEntry getUnit(String name) {
		return units.get(name);
	}
	
	public void addUnit(UnitEntry unit) {
		units.put(unit.getName(), unit);
	}
}
