package org.airgoons.goonfap.missiondata;

import com.opencsv.bean.CsvBindByName;

public class MissionRecord {
	@CsvBindByName(column = "Mission Time")
	private double time;
	@CsvBindByName(column = "Primary Object ID")
	private String primaryid;
	@CsvBindByName(column = "Primary Object Name")
	private String primaryname;
	@CsvBindByName(column = "Primary Object Coalition")
	private String primarycoalition;
	@CsvBindByName(column = "Primary Object Pilot")
	private String primarypilot;
	@CsvBindByName(column = "Event")
	private String event;
	@CsvBindByName(column = "Occurrences")
	private int occurrences;
	@CsvBindByName(column = "Secondary Object ID")
	private String secondaryid;
	@CsvBindByName(column = "Secondary Object Name")
	private String secondaryname;
	@CsvBindByName(column = "Secondary Object Coalition")
	private String secondarycoalition;
	@CsvBindByName(column = "Secondary Object Pilot")
	private String secondarypilot;
	@CsvBindByName(column = "Relevant Object ID")
	private String relevantid;
	@CsvBindByName(column = "Relevant Object Name")
	private String relevantname;
	@CsvBindByName(column = "Relevant Object Coalition")
	private String relevantcoalition;
	@CsvBindByName(column = "Relevant Object Pilot")
	private String relevantpilot;
	
	public MissionRecord() {
		time = 0;
		occurrences = 0;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public String getPrimaryid() {
		return primaryid;
	}

	public void setPrimaryid(String primaryid) {
		this.primaryid = primaryid;
	}

	public String getPrimaryname() {
		return primaryname;
	}

	public void setPrimaryname(String primaryname) {
		this.primaryname = primaryname;
	}

	public String getPrimarycoalition() {
		return primarycoalition;
	}

	public void setPrimarycoalition(String primarycoalition) {
		this.primarycoalition = primarycoalition;
	}

	public String getPrimarypilot() {
		return primarypilot;
	}

	public void setPrimarypilot(String primarypilot) {
		this.primarypilot = primarypilot;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public int getOccurrences() {
		return occurrences;
	}

	public void setOccurrences(int occurrences) {
		this.occurrences = occurrences;
	}

	public String getSecondaryid() {
		return secondaryid;
	}

	public void setSecondaryid(String secondaryid) {
		this.secondaryid = secondaryid;
	}

	public String getSecondaryname() {
		return secondaryname;
	}

	public void setSecondaryname(String secondaryname) {
		this.secondaryname = secondaryname;
	}

	public String getSecondarycoalition() {
		return secondarycoalition;
	}

	public void setSecondarycoalition(String secondarycoalition) {
		this.secondarycoalition = secondarycoalition;
	}

	public String getSecondarypilot() {
		return secondarypilot;
	}

	public void setSecondarypilot(String secondarypilot) {
		this.secondarypilot = secondarypilot;
	}

	public String getRelevantid() {
		return relevantid;
	}

	public void setRelevantid(String relevantid) {
		this.relevantid = relevantid;
	}

	public String getRelevantname() {
		return relevantname;
	}

	public void setRelevantname(String relevantname) {
		this.relevantname = relevantname;
	}

	public String getRelevantcoalition() {
		return relevantcoalition;
	}

	public void setRelevantcoalition(String relevantcoalition) {
		this.relevantcoalition = relevantcoalition;
	}

	public String getRelevantpilot() {
		return relevantpilot;
	}

	public void setRelevantpilot(String relevantpilot) {
		this.relevantpilot = relevantpilot;
	}

	@Override
	public String toString() {
		return "MissionRecord [time=" + time + ", primaryid=" + primaryid + ", primaryname=" + primaryname
				+ ", primarycoalition=" + primarycoalition + ", primarypilot=" + primarypilot + ", event=" + event
				+ ", occurrences=" + occurrences + ", secondaryid=" + secondaryid + ", secondaryname=" + secondaryname
				+ ", secondarycoalition=" + secondarycoalition + ", secondarypilot=" + secondarypilot + ", relevantid="
				+ relevantid + ", relevantname=" + relevantname + ", relevantcoalition=" + relevantcoalition
				+ ", relevantpilot=" + relevantpilot + "]";
	}
	
	
}
