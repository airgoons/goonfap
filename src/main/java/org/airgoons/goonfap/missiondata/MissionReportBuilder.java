package org.airgoons.goonfap.missiondata;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.airgoons.goonfap.database.UnitDatabase;
import org.airgoons.goonfap.database.UnitEntry;

public class MissionReportBuilder {
	private UnitDatabase db;
	private List<MissionRecord> records;

	// DCS written by dirty russians.
	private static final String BLUE_COALITION = "Enemies";
	private static final String RED_COALITION = "Allies";
	private static final NumberFormat DOLLAR_FORMAT = NumberFormat.getCurrencyInstance(Locale.US);

	public MissionReportBuilder(UnitDatabase db, List<MissionRecord> records) {
		super();
		this.db = db;
		this.records = records;
	}

	public String buildReportAll() {
		StringBuilder sb = new StringBuilder();
		
		
		sb.append("=Goon Flight Assessment Program (FAP) Report:\n");
		
		List<MissionRecord> tkrecords = records.stream().map(Function.identity()).filter(record -> record.getEvent().equals("HasBeenDestroyed"))
				.filter(record -> record.getPrimarycoalition().equals(record.getSecondarycoalition())).filter(record -> !record.getSecondarypilot().isEmpty()).collect(Collectors.toList());
		
		sb.append("==FRATRICIDE REPORT:\n");
		if(!tkrecords.isEmpty()) {
			tkrecords.forEach(tkrecord -> sb.append("At ").append(tkrecord.getTime()).append("s, ").append(tkrecord.getSecondarypilot()).append(" shot a ")
				.append(tkrecord.getPrimaryname()).append(" piloted by ").append(tkrecord.getPrimarypilot() != null ? tkrecord.getPrimarypilot() : "unknown").append("\n"));
		} else {
			sb.append("\tThere were none!\n");
		}
		
		sb.append("==Bluefor:\n");
		sb.append(buildReportCoalition(BLUE_COALITION));
		sb.append("==Redfor:\n");
		sb.append(buildReportCoalition(RED_COALITION));
		
		
		return sb.toString();
	}

	public StringBuilder buildReportCoalition(String coalition) {
		StringBuilder sb = new StringBuilder();
		//Get all assets the coalition has any record for.
		Map<String, UnitEntry> participants = records.stream().map(Function.identity())
				.filter(record -> record.getPrimarycoalition().equals(coalition))
				.collect(Collectors.toMap(MissionRecord::getPrimaryid, record -> dblookup(record.getPrimaryname()), (rec1, rec2) -> rec1));
		participants.putAll(records.stream().map(Function.identity())
				.filter(record -> record.getSecondarycoalition().equals(coalition)).collect(Collectors
						.toMap(MissionRecord::getSecondaryid, record -> dblookup(record.getSecondaryname()), (rec1, rec2) -> rec1)));
		participants.putAll(records.stream().map(Function.identity())
				.filter(record -> record.getRelevantcoalition().equals(coalition))
				.collect(Collectors.toMap(MissionRecord::getRelevantid, record -> dblookup(record.getRelevantname()), (rec1, rec2) -> rec1)));
		double totalassets = participants.values().stream().map(UnitEntry::getCost).reduce(0.0, Double::sum);

		//Get stats for each pilot.
		List<PilotStat> coalitionpilotstats = records.stream().map(Function.identity())
				.filter(record -> record.getPrimarycoalition().equals(coalition))
				.filter(record -> !record.getPrimarypilot().isEmpty()).map(MissionRecord::getPrimarypilot).distinct()
				.map(pilot->buildPilotStat(pilot)).collect(Collectors.toList());
		double lostcost = coalitionpilotstats.stream().map(PilotStat::getValueLost).reduce(0.0, Double::sum);
		double destroyedcost = coalitionpilotstats.stream().map(PilotStat::getValueDestroyed).reduce(0.0, Double::sum);
		
		
		//TODO: Now that we added the pilots, lets get the shit the non-pilots did for aggregate costs.
		
		

		sb.append("Committed ").append(DOLLAR_FORMAT.format(totalassets))
				.append(" in assets to the operation, expending ").append(DOLLAR_FORMAT.format(lostcost))
				.append(" to destroy ").append(DOLLAR_FORMAT.format(destroyedcost)).append("\n\n");

		coalitionpilotstats.forEach(pilotstat -> sb.append(pilotstat.toString()));

		return sb;
	}

	private PilotStat buildPilotStat(String name) {
		PilotStat ps = new PilotStat(name);

		//Can't just calculate deaths form "Destroyed" event, because that also includes logoffs.  So, if you took off and didn't land, you died.
		Map<String,List<MissionRecord>> torecords = records.stream().map(Function.identity())
				.filter(record -> record.getPrimarypilot().equals(name))
				.filter(record -> record.getEvent().equals("HasTakenOff")).collect(Collectors.groupingBy(MissionRecord::getPrimaryid,Collectors.toList()));
		Map<String,List<MissionRecord>> landingrecords = records.stream().map(Function.identity())
				.filter(record -> record.getPrimarypilot().equals(name))
				.filter(record -> record.getEvent().equals("HasLanded")).collect(Collectors.groupingBy(MissionRecord::getPrimaryid,Collectors.toList()));
		
		int takeoffs = torecords.entrySet().stream().map(Function.identity()).map(actos -> actos.getValue().size()).reduce(0, Integer::sum);
		int landings = landingrecords.entrySet().stream().map(Function.identity()).map(acldgs -> acldgs.getValue().size()).reduce(0, Integer::sum);
		ps.addDeaths(takeoffs-landings);
		if(ps.getDeaths() > 0) {
			for(Map.Entry<String,List<MissionRecord>> torecord : torecords.entrySet()) {
				String acid = torecord.getKey();
				List<MissionRecord> tolist = torecord.getValue();
				List<MissionRecord> ldglist = landingrecords.get(acid);
				int landingcount = 0;
				if(ldglist != null) {
					landingcount = ldglist.size();
				}
				int diff = tolist.size() - landingcount;
				double unitcost = dblookup(tolist.get(0).getPrimaryname()).getCost();
				ps.addValueLost(diff*unitcost);
			}
		}
		List<MissionRecord> killrecords = records.stream().map(Function.identity())
				.filter(record -> record.getSecondarypilot().equals(name))
				.filter(record -> record.getEvent().equals("HasBeenDestroyed")).collect(Collectors.toList());
		ps.addKills(killrecords.size());
		ps.addValueDestroyed(killrecords.stream().map(Function.identity())
				.map(record -> dblookup(record.getPrimaryname()).getCost()).reduce(0.0, Double::sum));

		ps.putAllMunitionsfired(records.stream().map(Function.identity())
				.filter(record -> record.getPrimarypilot().equals(name))
				.filter(record -> record.getEvent().equals("HasFired")).collect(Collectors.groupingBy(
						MissionRecord::getSecondaryname, Collectors.summingInt(MissionRecord::getOccurrences))));
		ps.addValueLost(ps.getMunitionsfired().entrySet().stream().map(Function.identity())
				.map(mapentry -> dblookup(mapentry.getKey()).getCost() * mapentry.getValue()).reduce(0.0, Double::sum));

		ps.putAllMunitionshit(records.stream().map(Function.identity())
				.filter(record -> record.getRelevantpilot().equals(name))
				.filter(record -> record.getEvent().equals("HasBeenHitBy")).collect(Collectors.groupingBy(
						MissionRecord::getSecondaryname, Collectors.summingInt(MissionRecord::getOccurrences))));

		return ps;
	}

	
	private UnitEntry dblookup(String name) {
		UnitEntry ret = db.getUnit(name);
		if (ret == null) {
			System.out.println("Unit of type " + name + " not found in database.");
			ret = new UnitEntry(name, "unknown", 0.0);
			db.addUnit(ret);
		}
		return ret;
	}
}
