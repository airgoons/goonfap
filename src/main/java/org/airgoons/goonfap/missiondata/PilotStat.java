package org.airgoons.goonfap.missiondata;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PilotStat {
	private static final NumberFormat DOLLAR_FORMAT = NumberFormat.getCurrencyInstance(Locale.US);
	private String name;
	private Map<String,Integer> munitionsfired;
	private Map<String,Integer> munitionshit;
	private int deaths;
	private int kills;
	private double valueLost;
	private double valueDestroyed;
	
	public PilotStat(String name) {
		this.name = name;
		munitionsfired = new HashMap<String,Integer>();
		munitionshit = new HashMap<String,Integer>();
		deaths = 0;
		kills = 0;
		valueLost = 0.0;
		valueDestroyed = 0.0;
	}
	
	public String getName() {
		return name;
	}
	
	public void putMunitionfired(String name, int amt) {
		Integer fired = munitionsfired.get(name);
		if(fired == null)
			fired = amt;
		else
			fired += amt;
		munitionsfired.put(name, fired);
	}
	
	public void putAllMunitionsfired(Map<String,Integer> newfires) {
		Map<String,Integer> combined = Stream.concat(munitionsfired.entrySet().stream(),newfires.entrySet().stream())
				.collect(Collectors.groupingBy(Map.Entry::getKey, Collectors.summingInt(Map.Entry::getValue)));
		munitionsfired = combined;
	}
	
	public Map<String, Integer> getMunitionsfired() {
		return munitionsfired;
	}
	
	public void putMunitionshit(String name, int amt) {
		Integer hit = munitionshit.get(name);
		if(hit == null) 
			hit = amt;
		else
			hit += amt;
		munitionshit.put(name, hit);
	}
	
	public void putAllMunitionshit(Map<String,Integer> newhits) {
		Map<String,Integer> combined = Stream.concat(munitionshit.entrySet().stream(),newhits.entrySet().stream())
				.collect(Collectors.groupingBy(Map.Entry::getKey, Collectors.summingInt(Map.Entry::getValue)));
		munitionshit = combined;
	}
	
	public Map<String, Integer> getMunitionshit() {
		return munitionshit;
	}
	
	public void addDeaths(int amt) {
		deaths += amt;
	}
	public int getDeaths() {
		return deaths;
	}
	
	public void addKills(int amt) {
		kills += amt;
	}
	public int getKills() {
		return kills;
	}

	public void addValueLost(double amt) {
		valueLost += amt;
	}
	
	public double getValueLost() {
		return valueLost;
	}

	public void addValueDestroyed(double amt) {
		valueDestroyed += amt;
	}
	
	public double getValueDestroyed() {
		return valueDestroyed;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("===Pilot: ").append(name).append("\n");
		sb.append("\tKilled: ").append(kills).append(" targets valued at ").append(DOLLAR_FORMAT.format(valueDestroyed)).append("\n");
		sb.append("\tExpended: ").append(DOLLAR_FORMAT.format(valueLost)).append(" over the course of the mission");
		if(deaths > 0)
			sb.append(", including ").append(deaths).append(" deaths.");
		sb.append("\n");
		sb.append("\tMunitions Employed:\n");
		munitionsfired.forEach((type,number) -> sb.append("\t\t").append(type).append(" Fired:").append(number).append(" Hit:").append(munitionshit.containsKey(type) ? munitionshit.get(type) : 0).append("\n"));
		return sb.toString();
	}
	
	
}
